<?php /* @var \App\Models\Todo[] $todos */ ?>

@props(['todos'])

<ul class="todo-list">
    @foreach($todos as $todo)
    <li class="todo-item">
        <form class="todo-item__form" action="{{route('todos.update', $todo)}}" method="post">
            @method('PUT')
            @csrf
            <input type="checkbox" name="done" {{ $todo->done ? 'checked' : null }}>
        </form>

        <div class="todo-item__content">
            <h3>{{ ucfirst($todo->title) }}</h3>
            <p>{{ $todo->content }}</p>


            <div class="todo-item__subtask">
                @if (count($todo->children) > 0)
                <h3> Sub-tasks: </h3>
                <x-todo-list :todos="$todo->children" />
                @endif
                <div>
                </div>
    </li>
    @endforeach
</ul>
