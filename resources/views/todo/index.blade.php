<?php /* @var \App\Models\Todo[] $todos */ ?>

@extends('layout.app')

@section('content')
<div class="container">
    @if(session('joke'))
    <div>
        {{ session('joke')}}
    </div>
    @endif

    <x-todo-list :todos="$todos->where('parent_id', null)" />

    <h2>Create To-do</h2>

    <form action="{{route('todos.store')}}" method="post" class="create-todo">
        @csrf
        <div class="create-todo__input-group">
            <label for="title">Title</label>
            <input id="title" type="text" name="title">
        </div>
        <div class="create-todo__input-group">
            <label for="content">Description</label>
            <textarea id="content" name="content" cols="30" rows="10"></textarea>
        </div>
        <div class="create-todo__input-group">
            <label for="content">Parent</label>
            <select name="parent_id" id="parent">
                <option value="" label="" />
                @foreach($todos as $todo)
                <option value="{{ $todo->id }}" label="{{ $todo->title }}" />
                @endforeach
            </select>
        </div>
        <button class="button" type="submit">Save</button>
    </form>
</div>
@endsection
