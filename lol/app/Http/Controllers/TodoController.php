<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        return view('todo.index', [
            'todos' => Todo::with('children')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
            'parent_id' =>
            function (string $attribute, mixed $value, Closure $fail) {
                if (empty($value)) {
                    return;
                }

                $parent = Todo::where('id', $value)->first();
                if ($parent === null) {
                    $fail("Incorrect parent");
                }
            }
        ]);

        Todo::create($validated);

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        $previousAbsoluteParentDoneStatus = $todo->absoluteParentIsDone();

        $todo->setDone($request->get('done') === 'on');
        $joke = '';

        if ($previousAbsoluteParentDoneStatus == false && $todo->absoluteParentIsDone()) {
            $response = Http::get("https://api.chucknorris.io/jokes/random")->json();
            $joke = $response['value'] ?? null;
        }

        return response($joke);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        //
    }
}
