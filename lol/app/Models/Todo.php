<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Todo
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $done
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Todo> $children
 * @property-read int|null $children_count
 * @property-read Todo|null $parent
 * @method static \Database\Factories\TodoFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Todo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Todo query()
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereDone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereUpdatedAt($value)
 * @property int|null $parent_id
 * @method static \Illuminate\Database\Eloquent\Builder|Todo whereParentId($value)
 * @mixin \Eloquent
 */
class Todo extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function children()
    {
        return $this->hasMany(Todo::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Todo::class, 'parent_id');
    }

    /**
     * Update the done column to the value,
     * Recursively marks the parent Todo item as done if all children are done as well
     *
     * @param  bool  $value value to set
     * @return bool  if value is properly set
     */
    public function setDone(bool $value): bool
    {
        $succes = $this->update(['done' => $value]);

        abort_unless($succes, 500, "Something went wrong");

        if ($this->parent?->children !== null) {

            if ($this->allChildrenAreDone()) {
                // if recursively
                $succesfull = $this->parent->setDone(true);
                // else
                // $succes = $this->parent->update(['done' => $value]);

                // unset: Depends if we wants to unset parent as done
            } else {
                $succesfull = $this->parent->setDone(false);
                // unset: end
            }

            return $succesfull;
        }

        return true;
    }

    public function absoluteParentIsDone(): bool
    {
        $absoluteParent = $this->getAbsoluteParent($this);

        return $absoluteParent?->done;
    }

    private function getAbsoluteParent(Todo $todo): Todo
    {
        if ($todo->parent === null) {
            return $todo;
        } else {
            return $this->getAbsoluteParent($todo->parent);
        }
    }

    private function allChildrenAreDone()
    {
        $children = $this->parent?->children()->get(['done']);
        if (!$children) {
            return true;
        }

        $amountOfChildrenDone = $children->where('done', 1)->count();

        return $amountOfChildrenDone === $children->count();
    }
}
